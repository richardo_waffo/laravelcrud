@include('inc.header')
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <form class="form-horizontal" method="POST" action=" {{url('/insert')}} ">
            {{csrf_field()}}
            <legend>Mon Formulaire Avec Laravel</legend>
            @if(count($errors) > 0)
              @foreach($errors->all() as $error)
                <div class="alert alert-danger">
                {{$error}}
                </div>
              @endforeach
            @endif
            <div class="form-group">
              <label for="exampleFormControlInput1">Title</label>
              <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Title">
            </div>
            
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Description</label>
              <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{url('/')}}" class="btn btn-default">Back</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@include('inc.footer')
